package me.scholtes.bossraids;

import org.bukkit.plugin.java.JavaPlugin;

import me.scholtes.bossraids.commands.BossLocationCMD;
import me.scholtes.bossraids.commands.OpenGUICMD;
import me.scholtes.bossraids.commands.SetLocationCMD;
import me.scholtes.bossraids.commands.StartRaidCMD;
import me.scholtes.bossraids.events.InventoryClick;
import me.scholtes.bossraids.events.MobDeath;

public class BossRaids extends JavaPlugin {
	
	private Utils utils = new Utils();
	
	public void onEnable() {
		
		getServer().getPluginManager().registerEvents(new InventoryClick(utils, this), this);
		getServer().getPluginManager().registerEvents(new MobDeath(utils, this), this);
		
		getCommand("bossraid").setExecutor(new OpenGUICMD(utils, this));
		getCommand("setraid").setExecutor(new SetLocationCMD(utils));
		getCommand("setboss").setExecutor(new BossLocationCMD(utils));
		getCommand("startraid").setExecutor(new StartRaidCMD(utils, this));
		
		getConfig().options().copyDefaults(true);
		saveConfig();
	}

}
