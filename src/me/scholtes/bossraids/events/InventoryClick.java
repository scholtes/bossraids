package me.scholtes.bossraids.events;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import io.lumine.xikage.mythicmobs.MythicMobs;
import io.lumine.xikage.mythicmobs.api.exceptions.InvalidMobTypeException;
import me.scholtes.bossraids.BossRaids;
import me.scholtes.bossraids.Utils;

public class InventoryClick implements Listener {
	
	private Utils utils;
	private BossRaids plugin;
	
	public InventoryClick(Utils utils, BossRaids plugin) {
		this.utils = utils;
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		
		File data = new File("plugins/BossRaids/", "raids.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		
		if (!e.getInventory().getName().equals(utils.color("&4&lBoss Raids"))) {
			return;
		}
		e.setCancelled(true);
		ItemStack clickedItem = e.getCurrentItem();
		ItemMeta meta = clickedItem.getItemMeta();
		if (meta.getDisplayName() == null) {
			return;
		}
		if (!data.exists()) {
			return;
		}
		World world = Bukkit.getWorld(cfg.getString("raid.world"));
		double x = cfg.getDouble("raid.x");
		double y = cfg.getDouble("raid.y");
		double z = cfg.getDouble("raid.z");
		float yaw = (float) cfg.getDouble("raid.yaw");
		float pitch = (float) cfg.getDouble("raid.pitch");
		Location raidLocation = new Location(world, x, y, z, yaw, pitch);
		if (meta.getDisplayName().equals(utils.color("&a&lStart boss raid"))) {
			double xBoss = cfg.getDouble("raid.xboss");
			double yBoss = cfg.getDouble("raid.yboss");
			double zBoss = cfg.getDouble("raid.zboss");
			World worldBoss = Bukkit.getWorld(cfg.getString("raid.worldboss"));
			Location bossLocation = new Location(worldBoss, xBoss, yBoss, zBoss);
			List<String> inRaid = new ArrayList<String>();
			inRaid.add(e.getWhoClicked().getUniqueId().toString());
			cfg.set("raid.players", inRaid);
			cfg.set("raid.lastuse", null);
			try {
				cfg.save(data);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.getWhoClicked().closeInventory();
			for (Entity entity : e.getWhoClicked().getNearbyEntities(10, 10, 10)) {
				if (entity instanceof Player) {
					((Player) entity).closeInventory();
				}
			}
			e.getWhoClicked().teleport(raidLocation);
			Bukkit.broadcastMessage(utils.color("&6&lRaid Master&r &8> &f&nA raid has started! Go to &e&n/warp raid&f&n to join!"));
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tm broadcast &6&l&nBoss raid started\\n&fGo to &6/warp raid &fto participate");
			Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {
					List<String> bosses = plugin.getConfig().getStringList("boss-list");
					Collections.shuffle(bosses);
					try {
						MythicMobs.inst().getAPIHelper().spawnMythicMob(bosses.get(0), bossLocation);
					} catch (InvalidMobTypeException e) {
						e.printStackTrace();
					}
				}
			}, 70L);
			return;
			
		}
		if (meta.getDisplayName().equals(utils.color("&6&lBoss raid in progress"))) {
			List<String> inRaid = cfg.getStringList("raid.players");
			inRaid.add(e.getWhoClicked().getUniqueId().toString());
			cfg.set("raid.players", inRaid);
			try {
				cfg.save(data);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.getWhoClicked().closeInventory();
			e.getWhoClicked().teleport(raidLocation);
			e.getWhoClicked().sendMessage(utils.color("&6Raid Master &8> &fYou have joined the raid!"));
			return;
		}
		
	}
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		leaveRaid(e.getEntity());
	}
	
	@EventHandler
	public void onDisconnect(PlayerQuitEvent e) {
		leaveRaid(e.getPlayer());
	}
	
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e) {
		Player p = e.getPlayer();
		File data = new File("plugins/BossRaids/", "raids.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		
		if (p.isOp()) return;
		if (e.getMessage().equals("raid leave")) return;
		if (!data.exists() || cfg.getStringList("raid.players") == null) return;
		if (!cfg.getStringList("raid.players").contains(p.getUniqueId().toString())) return;
		
		e.setCancelled(true);
		p.sendMessage(utils.color("&eRaid Master &8> &fYou cannot run commands while in a raid. To leave the raid do &6/raid leave&f."));
	}
	
	
	private void leaveRaid(Player p) {
		File data = new File("plugins/BossRaids/", "raids.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		if (!data.exists() || cfg.getStringList("raid.players") == null) {
			return;
		}
		if (!cfg.getStringList("raid.players").contains(p.getUniqueId().toString())) {
			return;
		}
		List<String> inRaid = cfg.getStringList("raid.players");
		if (inRaid.size() == 1) {
			cfg.set("raid.lastuse", System.currentTimeMillis() + 3600000);
			cfg.set("raid.players", null);
			Bukkit.broadcastMessage(utils.color("&6&lRaid Master&r &8> &f&nRaid ended! The players have been defeated!"));
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tm broadcast &6&l&nRaid ended\\n&fThe players were defeated! The next raid will be in 1 hour!");
			try {
				cfg.save(data);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			return;
		}
		inRaid.remove(p.getUniqueId().toString());
		cfg.set("raid.players", inRaid);
		try {
			cfg.save(data);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "spawn " + p.getName());
	}

}
