package me.scholtes.bossraids.events;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import io.lumine.xikage.mythicmobs.api.bukkit.events.MythicMobDeathEvent;
import me.scholtes.bossraids.BossRaids;
import me.scholtes.bossraids.Utils;

public class MobDeath implements Listener {
	
	private Utils utils;
	private BossRaids plugin;
	
	public MobDeath(Utils utils, BossRaids plugin) {
		this.utils = utils;
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onMobDeath(MythicMobDeathEvent e) {
		
		File data = new File("plugins/BossRaids/", "raids.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		
		List<String> bosses = plugin.getConfig().getStringList("boss-list");
		if (!bosses.contains(e.getMob().getType().toString())) {
			return;
		}
		cfg.set("raid.lastuse", System.currentTimeMillis() + 3600000);
		try {
			cfg.save(data);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		Bukkit.broadcastMessage(utils.color("&6&lRaid Master&r &8> &f&nRaid ended! The boss has been defeated!"));
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tm broadcast &6&l&nRaid ended\\n&fThe boss was defeated! The next raid will be in 1 hour!");
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				List<String> cmds = plugin.getConfig().getStringList("reward-list");
				for (String s : cfg.getStringList("raid.players")) {
					Player p = Bukkit.getPlayer(UUID.fromString(s));
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "spawn " + p.getName());
					Collections.shuffle(cmds);
					int min = plugin.getConfig().getInt("minimum");
					int max = plugin.getConfig().getInt("maximum");
					Random rand = new Random();
					int random = rand.nextInt((max - min) + 1) + min;
					for (int i = 0; i < random; i++) {
						Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmds.get(i).replace("%player%", p.getName()));
					}
				}
				cfg.set("raid.players", null);
				try {
					cfg.save(data);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}, 60L);
		
	}

}
