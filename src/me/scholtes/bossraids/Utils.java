package me.scholtes.bossraids;

import org.bukkit.ChatColor;

public class Utils {

	public String color(String text) {
		return ChatColor.translateAlternateColorCodes('&', text);
	}
	
}
