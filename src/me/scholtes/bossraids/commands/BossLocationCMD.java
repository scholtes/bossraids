package me.scholtes.bossraids.commands;

import java.io.File;
import java.io.IOException;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import me.scholtes.bossraids.Utils;

public class BossLocationCMD implements CommandExecutor {

    private Utils utils;
	
	public BossLocationCMD(Utils utils) {
		this.utils = utils;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		File data = new File("plugins/BossRaids/", "raids.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		
		if (!(sender instanceof Player)) {
			sender.sendMessage(utils.color("&cYou need to be player"));
			return true;
		}
		
		Player p = (Player) sender;
		
		if (!sender.isOp()) {
			sender.sendMessage(utils.color("&cYou cannot do this!"));
			return true;
		}
		sender.sendMessage(utils.color("&aBoss location set"));
		cfg.set("raid.xboss", p.getLocation().getX());
		cfg.set("raid.yboss", p.getLocation().getY());
		cfg.set("raid.zboss", p.getLocation().getZ());
		cfg.set("raid.worldboss", p.getWorld().getName());
		try {
			cfg.save(data);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return true;
	}

}
