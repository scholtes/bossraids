package me.scholtes.bossraids.commands;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import io.lumine.xikage.mythicmobs.MythicMobs;
import io.lumine.xikage.mythicmobs.api.exceptions.InvalidMobTypeException;
import me.scholtes.bossraids.BossRaids;
import me.scholtes.bossraids.Utils;

public class StartRaidCMD implements CommandExecutor {

    private Utils utils;
    private BossRaids plugin;
	
	public StartRaidCMD(Utils utils, BossRaids plugin) {
		this.plugin = plugin;
		this.utils = utils;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if (!(sender instanceof Player)) {
			sender.sendMessage(utils.color("&cYou need to be player"));
			return true;
		}
		
		Player p = (Player) sender;
		
		if (!sender.isOp()) {
			sender.sendMessage(utils.color("&cYou cannot do this!"));
			return true;
		}
		File data = new File("plugins/BossRaids/", "raids.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		World world = Bukkit.getWorld(cfg.getString("raid.world"));
		double x = cfg.getDouble("raid.x");
		double y = cfg.getDouble("raid.y");
		double z = cfg.getDouble("raid.z");
		float yaw = (float) cfg.getDouble("raid.yaw");
		float pitch = (float) cfg.getDouble("raid.pitch");
		Location raidLocation = new Location(world, x, y, z, yaw, pitch);
		double xBoss = cfg.getDouble("raid.xboss");
		double yBoss = cfg.getDouble("raid.yboss");
		double zBoss = cfg.getDouble("raid.zboss");
		World worldBoss = Bukkit.getWorld(cfg.getString("raid.worldboss"));
		Location bossLocation = new Location(worldBoss, xBoss, yBoss, zBoss);
		List<String> inRaid = new ArrayList<String>();
		inRaid.add(p.getUniqueId().toString());
		cfg.set("raid.players", inRaid);
		cfg.set("raid.lastuse", null);
		try {
			cfg.save(data);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		p.teleport(raidLocation);
		Bukkit.broadcastMessage(utils.color("&6&lRaid Master&r &8> &f&nA raid has started! Go to &e&n/warp raid&f&n to join!"));
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tm broadcast &6&l&nBoss raid started\\n&fGo to &6/warp raid &fto participate");
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				List<String> bosses = plugin.getConfig().getStringList("boss-list");
				Collections.shuffle(bosses);
				try {
					MythicMobs.inst().getAPIHelper().spawnMythicMob(bosses.get(0), bossLocation);
				} catch (InvalidMobTypeException e) {
					e.printStackTrace();
				}
			}
		}, 70L);
		
		return true;
		
	}

}
