package me.scholtes.bossraids.commands;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.scholtes.bossraids.BossRaids;
import me.scholtes.bossraids.Utils;

public class OpenGUICMD implements CommandExecutor {
	
	private BossRaids plugin;
	private Utils utils;
	
	public OpenGUICMD(Utils utils, BossRaids plugin) {
		this.utils = utils;
		this.plugin = plugin;
	}
	
    private Inventory inv = Bukkit.createInventory(null, 27, ChatColor.translateAlternateColorCodes('&', "&4&lBoss Raids"));

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		File data = new File("plugins/BossRaids/", "raids.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		
		if(!(sender instanceof Player)) {
			sender.sendMessage(utils.color("&cYou need to be player"));
			return true;
		}
		
		Player p = (Player) sender;
		
		if (args.length == 1 && args[0].equals("leave")) {
			if (!data.exists() || cfg.getStringList("raid.players") == null) {
				p.sendMessage(utils.color("&6Raid Master &8> &fYou are not currently in a raid!"));
				return true;
			}
			if (!cfg.getStringList("raid.players").contains(p.getUniqueId().toString())) {
				p.sendMessage(utils.color("&6Raid Master &8> &fYou are not currently in a raid!"));
				return true;
			}
			List<String> inRaid = cfg.getStringList("raid.players");
			if (inRaid.size() == 1) {
				cfg.set("raid.lastuse", System.currentTimeMillis() + 60000);
				cfg.set("raid.players", null);
				Bukkit.broadcastMessage(utils.color("&6&lRaid Master&r &8> &f&nRaid ended! The players have been defeated!"));
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tm broadcast &6&l&nRaid ended\\n&fThe players were defeated! The next raid will be in 1 hour!");
				try {
					cfg.save(data);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				return true;
			}
			inRaid.remove(p.getUniqueId().toString());
			cfg.set("raid.players", inRaid);
			try {
				cfg.save(data);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			p.sendMessage(utils.color("&6Raid Master &8> &fYou have left the raid!"));
			Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "spawn " + p.getName());
			return true;
		}
		if (args.length == 1 && args[0].equals("reload")) {
			if (!p.isOp()) {
				p.sendMessage(utils.color("&cYou cannot do this!"));
				return true;
			}
			p.sendMessage(utils.color("&aReloading the config!"));
			plugin.reloadConfig();
			return true;
		}
		
		List<String> lore = new ArrayList<String>();
		for (int i = 0; i < 27; i++) {
			inv.setItem(i, guiItem("", lore, Material.STAINED_GLASS_PANE, (short) 15));
		}
		if (data.exists() && cfg.getString("raid.lastuse") != null) {
			long timeLeft = Long.parseLong(cfg.getString("raid.lastuse")) - System.currentTimeMillis();
			if (timeLeft > 0) {
				lore.add(utils.color(""));
				lore.add(utils.color("&7Next boss raid will be"));
				lore.add(utils.color("&7available in &7&n" + (int) Math.ceil(timeLeft / 1000.0 / 60.0) + " minutes"));
				inv.setItem(13, guiItem(utils.color("&c&lBoss raid unavailable"), lore, Material.WATCH, (short) 0));
				p.openInventory(inv);
				return true;
			}
			lore.add(utils.color(""));
			lore.add(utils.color("&7&nClick to start a boss raid"));
			inv.setItem(13, guiItem(utils.color("&a&lStart boss raid"), lore, Material.MONSTER_EGG, (short) 0));
			p.openInventory(inv);
			return true;
		}
		lore.add(utils.color(""));
		lore.add(utils.color("&7&nClick to join the boss raid"));
		inv.setItem(13, guiItem(utils.color("&6&lBoss raid in progress"), lore, Material.DIAMOND_SWORD, (short) 0));
		
		p.openInventory(inv);
		return true;
	}
	
	public ItemStack guiItem(String name, List<String> desc, Material mat, short data) {
    	ItemStack i = new ItemStack(mat, 1, data);
        ItemMeta iMeta = i.getItemMeta();
        iMeta.setDisplayName(name);
        iMeta.setLore(desc);
        i.setItemMeta(iMeta);
        return i;
    }

}
